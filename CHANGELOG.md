# webdrivertorso.sh Changelog
All notable changes to this project will be documented in this file.

## 0.0.2 - 2015-12-13
### Changed
- Removed python dependancy by replacing random number generation with built in `$RANDOM` property
- Switched to using `mktemp` command to generate file names

### Added
- New argument:
    * -t to set text appearing before Slide numbers

## 0.0.1 - 2015-12-13
### Added
- Initial commit
- webdrivertorso.sh generates test videos